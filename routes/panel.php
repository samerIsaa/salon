<?php


Route::group(['prefix' => 'panel', 'as' => 'panel.'], function () {

    Route::get('/' , function (){
        return redirect(route('panel.showLogin'));
    });

    Route::get('login', 'Auth\LoginController@showLoginForm')->name('showLogin');
    Route::post('login', 'Auth\LoginController@login')->name('login');

    /*
     * Reset Password Routes
     */
    Route::group(['prefix' => 'password/' , 'as'    => 'password.' , 'namespace'=>'Auth'], function () {

        Route::post('email' , ['as' => 'email' , 'uses'=>'ForgotPasswordController@sendResetLinkEmail']);
        Route::get('reset/{token}' , 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
        Route::post('reset' , 'Auth\ResetPasswordController@reset')->name('password.update');
    });


    Route::group(['middleware' => 'auth:admin'], function () {

        Route::get('logout' , ['as' => 'logout' , 'uses' => 'Auth\LoginController@logout']);


        Route::post('upload', 'ImageController@upload')->name('index');
        Route::post('delete', 'ImageController@delete')->name('index');


        Route::get('index' , 'HomeController@index')->name('index');


        Route::resource('admins' , 'AdminController')->except(['show']);
        Route::get('admins/datatable' , ['as'=> 'admins.datatable','uses'=>'AdminController@datatable']);

        Route::resource('users' , 'UserController')->except(['show']);
        Route::get('users/datatable' , 'UserController@datatable');


        Route::resource('services' , 'ServiceController')->except(['show']);
        Route::get('services/datatable' , 'ServiceController@datatable');


        Route::resource('offers' , 'OfferController')->except(['show']);
        Route::get('offers/datatable' , 'OfferController@datatable');

        Route::resource('vacancies' , 'VacancyController')->except(['show', 'create' , 'edit']);
        Route::get('vacancies/datatable' , 'VacancyController@datatable');
        Route::post('vacancies/deleteSelected' , 'VacancyController@deleteSelected')->name('vacancies.deleteSelected');


        Route::group(['prefix' => 'contacts/', 'as' => 'contacts.'], function () {
            Route::get('/', ['as'=> 'index','uses'=>'ContactController@index']);
            Route::get('/datatable' , ['as'=> 'datatable','uses'=>'ContactController@datatable']);
            Route::get('/{id}', ['as'=> 'destroy','uses'=>'ContactController@show']);
            Route::delete('/{id}', ['as'=> 'destroy','uses'=>'ContactController@destroy']);

            Route::get('/{id}/show-replay' , 'ContactController@showReplay');
            Route::post('add-replay' , ['as'=> 'reply','uses'=>'ContactController@reply']);

        });




        Route::resource('pages' , 'PageController')->except(['destroy']);

        Route::resource('settings' , 'SettingsController')->except(['create' , 'edit' , 'show' , 'destroy']);

        Route::resource('slider' , 'SliderController')->except(['show' , 'edit' , 'update' ]);
        Route::post('slider/update-visiblity/{id}' , 'SliderController@updateVisiblity')->name('slider.updateVisiblity');


    });


});
