<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::get('/check', ['as'=>'checkDomain' , 'uses'  => 'CheckController@checkDomain']);


Route::get('/who-we-are', 'PageController@whoWeAre');
Route::get('/privacy', 'PageController@privacy');
Route::get('/usagePolicy', 'PageController@usagePolicy');

Route::post('contact-us', 'ContactController@send');
Route::get('info', 'HomeController@getInfo');


Route::get('faq', 'FaqController@index');

Route::get('extenstion', 'ExtensionController@extension');
Route::get('socials', 'ExtensionController@socials');


