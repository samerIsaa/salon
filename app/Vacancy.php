<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vacancy extends Model
{
    protected $fillable = [
        'title'
    ];

    public function scopeFilter($q , $keySearch)
    {
        return $q->where('title' , 'like' , '%'.$keySearch . '%');
    }
}
