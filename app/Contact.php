<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'name' , 'email' , 'message' , 'subject' , 'phone', 'read_at'
    ];

    public function replies()
    {
        return $this->hasMany(Replay::class);
    }
}
