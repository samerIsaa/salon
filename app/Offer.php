<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Offer extends Model
{
    protected $fillable = [
        'service_id', 'name', 'description', 'image', 'ended_at',
        'original_price', 'offer_price'
    ];
    protected $appends = ['short_description'];

    public function getShortDescriptionAttribute()
    {
        return Str::limit(strip_tags($this->description), 40);
    }


    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    public function scopeFilter($q, $keySearch)
    {
        return $q->where('name', 'like', '%' . $keySearch . '%')
            ->orWhere('original_price', 'like', '%' . $keySearch . '%')
            ->orWhere('offer_price', 'like', '%' . $keySearch . '%');
    }

}
