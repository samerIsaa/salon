<?php

namespace App\Http\Controllers\Front;


use App\Offer;
use App\Service;
use App\Slider;
use App\Http\Controllers\Controller;

class OfferController extends Controller
{

    public function index()
    {
        $data['offers'] = Offer::paginate(2);

        return view('front.offers' , $data);
    }


}
