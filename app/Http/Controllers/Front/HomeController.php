<?php

namespace App\Http\Controllers\Front;


use App\Offer;
use App\Page;
use App\Service;
use App\Slider;
use App\Http\Controllers\Controller;
use App\Vacancy;

class HomeController extends Controller
{

    public function home()
    {
        $data['sliders'] = Slider::where('isVisible' , 1)->get();
        $data['services'] = Service::all();
        $data['offers'] = Offer::latest()->take(3)->get();
        $data['vacancies'] = Vacancy::all();

        $data['about'] = Page::find(1);
        return view('front.index' , $data);
    }


}
