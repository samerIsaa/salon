<?php

namespace App\Http\Controllers\Front;

use App\Contact;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

class ContactController extends Controller
{

    public function index()
    {
        return view('front.contact');
    }

    public function send(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'name'   => 'required|string',
            'email'   => 'required|email',
            'phone'   => 'required',
            'message'   => 'required|string',
        ]);
        $validator->setAttributeNames($this->niceNames());
        if ($validator->fails()) {
            return response()->json([
                'msg' => $validator->errors()->first()
            ], 422);
        }

        Contact::create($data);

        return response()->json([
            'msg' => __('front.msgSent')
        ], 200);
    }

    private function niceNames(){
        return [
            'name'   => __('front.name'),
            'email'   => __('front.email'),
            'message'   => __('front.contactMsg'),
            'phone'   =>  __('front.phone'),

        ];
    }
}
