<?php

namespace App\Http\Controllers\Panel;

use App\Admin;
use App\Http\Controllers\Controller;
use App\Offer;
use App\Service;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class OfferController extends Controller
{
    public function index()
    {

        return view('panel.offers.index');
    }

    public function create()
    {
        $data['services'] = Service::all();
        return view('panel.offers.create', $data);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'required|string',
            'description' => 'required|string',
            'service_id' => 'required|exists:services,id',
            'original_price' => 'nullable|numeric',
            'offer_price' => 'nullable|numeric',
            'ended_at' => 'nullable|date',
            'image' => 'required|image',
        ]);

        $validator->setAttributeNames($this->niceNames());

        if ($validator->fails()) {
            return response()->json([
                'status' => 422,
                'msg' => $validator->errors()->first()
            ], 422);
        }

        if ($avatar = $request->file('image')) {
            $data['image'] = $avatar->store('offers');
        }

        if ($request->filled('ended_at')) {
            $data['ended_at'] = Carbon::parse($data['ended_at'])->format('Y-m-d');
        }else{
            $data['ended_at'] = null;
        }

        Offer::create($data);
        return response()->json([
            'status' => 200,
            'msg' => 'تمت العملية بنجاح'
        ], 200);
    }


    public function edit($id)
    {
        $data['services'] = Service::all();
        $data['offer'] = Offer::findOrFail($id);

        return view('panel.offers.edit' , $data);
    }

    public function update(Request $request, $id)
    {
        $offer = Offer::findOrFail($id);
        $data = $request->all();


        $validator = Validator::make($data, [
            'name' => 'required|string',
            'description' => 'required|string',
            'service_id' => 'required|exists:services,id',
            'original_price' => 'nullable|numeric',
            'offer_price' => 'nullable|numeric',
            'ended_at' => 'nullable|date',
            'image' => 'image',
        ]);
        $validator->setAttributeNames($this->niceNames());

        if ($validator->fails()){
            return response()->json([
                'status' => 422,
                'msg'   => $validator->errors()->first()
            ],422);
        }

        if ($avatar = $request->file('image')) {
            Storage::delete($offer->image);
            $data['image'] = $avatar->store('offers');
        }

        if ($request->filled('ended_at')) {
            $data['ended_at'] = Carbon::parse($data['ended_at'])->format('Y-m-d');
        }else{
            $data['ended_at'] = null;
        }


        $offer->update($data);
        return response()->json([
            'status' => 200,
            'msg'   => 'تمت العملية بنجاح'
        ],200);
    }

    public function destroy($id)
    {
        try{
            $offer = Offer::findOrFail($id);

            if ($offer->image){
                Storage::delete($offer->image);
            }
            $offer->delete();
            return response()->json([
                'status' => 200,
                'msg'   => 'تمت العملية بنجاح'
            ], 200);
        }catch (\Exception $exception){
            return response()->json([
                'status' => 500,
                'msg'   => 'لقد حدث خطأ ما'
            ], 500);
        }

    }

    public function datatable()
    {
        $pagination = Input::get('pagination');
        $query = Input::get('query');
        $search = $query['generalSearch'];



        if ($pagination['perpage'] == -1 || $pagination['perpage'] == null) {
            $pagination['perpage'] = 10;

        }
        $items = Offer::orderByDesc('created_at')->with('service');


        if ($search != null) {
            $items = $items->filter($search);
        }

        $itemsCount = $items->count();
        $items = $items->take($pagination['perpage'])->skip($pagination['perpage'] *($pagination['page']-1) )->get();
        $pagination['total'] = $itemsCount;
        $pagination['pages'] = ceil($itemsCount / $pagination['perpage']);

        $data['meta'] = $pagination;
        $data['data'] = $items;
        return $data;
    }


    private function niceNames()
    {
        return [
            'name' => 'الإسم',
            'description' => 'الوصف',
            'service_id' => 'الخدمة',
            'original_price' => 'السعر قبل العرض',
            'offer_price' => 'سعر العرض',
            'ended_at' => 'تاريخ الإنتهاء',
            'image' => 'صورة العرض',
        ];
    }

}
