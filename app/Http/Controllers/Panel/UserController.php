<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Validator;

class UserController extends Controller
{


    private function niceNames()
    {
        return [
            'full_name' => 'الإسم',
            'phone' => 'رقم الهاتف',
            'email' => 'البريد الإلكتروني',
            'password' => 'كلمة المرور',
        ];
    }



    public function index()
    {
        return view('panel.users.index');
    }

    public function create()
    {
        return view('panel.users.create');
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'full_name' => 'required|string|regex:/^[\pL\s\-]+$/u',
            'phone' => 'required|numeric|unique:users,phone',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:8',
        ]);
        $validator->setAttributeNames($this->niceNames());

        if ($validator->fails()) {
            return response()->json([
                'msg' => $validator->errors()->first()
            ], 422);
        }

        if ($avatar = $request->file('avatar')){
            $data['avatar'] = $avatar->store('users');
        }
        $data['password'] = Hash::make($data['password']);

        User::create($data);
        return response()->json([
            'msg' => 'تمت عملية الإضافة بنجاح'
        ], 200);
    }

    public function edit($id)
    {
        $data['user'] = User::findOrFail($id);
        return view('panel.users.edit', $data);
    }


    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $data = $request->all();

        if(!$request->filled('password')){
            unset($data['password']);
        }


        $validator = Validator::make($data, [
            'full_name' => 'required|string|regex:/^[\pL\s\-]+$/u',
            'phone' => 'required|numeric|unique:users,phone,' . $id,
            'email' => 'required|email|unique:users,email,' . $id,
            'password' => 'nullable|min:8'
        ]);

        $validator->setAttributeNames($this->niceNames());

        if ($validator->fails()) {
            return response()->json([
                'msg' => $validator->errors()->first()
            ], 422);
        }

        if ($avatar = $request->file('avatar')){
            Storage::delete($user->avatar);
            $data['avatar'] = $avatar->store('users');
        }
        if ($request->filled('password')){
            $data['password'] = Hash::make($data['password']);
        }
        $user->update($data);

        return response()->json([
            'msg' => 'تمت عملية التعديل بنجاح'
        ], 200);
    }

    public function destroy($id)
    {
        try{
            $user = User::findOrFail($id);

            $user->delete();
            return response()->json([
                'status' => 200,
                'msg' => 'لقد تمت عملية الحذف بنجاح'
            ], 200);
        }catch (\Exception $exception){
            return response()->json([
                'status' => 500,
                'msg' => 'لقد حدث خطأ ما'
            ], 500);
        }
    }


    public function datatable()
    {
        $pagination = Input::get('pagination');
        $query = Input::get('query');

//        $data['name'] = $query['name'];
//        $data['email'] = $query['email'];
//        $data['phone'] = $query['phone'];
//        $data['ssn'] = $query['ssn'];

        $sort = Input::get('sort');


        if ($pagination['perpage'] == -1 || $pagination['perpage'] == null) {
            $pagination['perpage'] = 10;
        }

        $items = User::query();

//        if ($data['name'] != null) {
//            $items->where('name', 'like', '%' . $data['name'] . '%');
//        }
//
//        if ($data['email'] != null) {
//            $items->where('email', 'like', '%' . $data['email'] . '%');
//        }
//
//        if ($data['phone'] != null) {
//            $items->where('phone', 'like', '%' . $data['phone'] . '%');
//        }
//
//        if ($data['ssn'] != null) {
//            $items->where('ssn', $data['ssn']);
//        }

        if ($sort && count($sort)){
            $items->orderBy($sort['field'] , $sort['sort']);
        }else{
            $items->orderByDesc('created_at');
        }

        $itemsCount = $items->count();
        $items = $items->take($pagination['perpage'])->skip($pagination['perpage'] * ($pagination['page'] - 1))->get();
        $pagination['total'] = $itemsCount;
        $pagination['pages'] = ceil($itemsCount / $pagination['perpage']);

        $data['meta'] = $pagination;
        $data['data'] = $items;
        return $data;
    }
}
