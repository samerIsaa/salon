<?php

namespace App\Http\Controllers\Panel;

use App\Faq;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use Validator;

class FaqController extends Controller
{


    private function niceNames()
    {
        return [
            'ar.title' => 'العنوان باللغة العربية',
            'en.title' => 'العنوان باللغة الانجليزية',
            'ar.description' => 'الوصف باللغة الانجليزية',
            'en.description' => 'الوصف باللغة الانجليزية',
        ];
    }

    private function validation($data)
    {
        $validator = Validator::make($data, [
            'ar.title' => 'required|string',
            'ar.description' => 'required|string',
            'en.title' => 'required|string',
            'en.description' => 'required|string',
        ]);

        $validator->setAttributeNames($this->niceNames());

        return $validator;
    }

    public function index()
    {
        return view('panel.faqs.index');
    }



    public function create()
    {
        return view('panel.faqs.create');
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $validator = $this->validation($data);

        if ($validator->fails()) {
            return response()->json([
                'status' => 422,
                'msg' => $validator->errors()->first()
            ], 422);
        }

        Faq::create($data);
        return response()->json([
            'status' => 200,
            'msg' => 'تمت العملية بنجاح'
        ], 200);

    }


    public function edit($id)
    {
        $faq = Faq::findOrFail($id);
        return view('panel.faqs.edit', compact('faq'));
    }


    public function update(Request $request, $id)
    {
        $faq = Faq::findOrFail($id);
        $data = $request->all();

        $validator = $this->validation($data);

        if ($validator->fails()) {
            return response()->json([
                'status' => 422,
                'msg' => $validator->errors()->first()
            ], 422);
        }

        $data = [
            'en' => $data['en'],
            'ar' => $data['ar'],
        ];

        $faq->update($data);
        return response()->json([
            'status' => 200,
            'msg' => 'تمت العملية بنجاح'
        ], 200);
    }


    public function destroy($id)
    {
        try {
            $faq = Faq::destroy($id);

            return response()->json([
                'status' => 200,
                'msg' => 'تمت العملية بنجاح'
            ], 200);
        } catch (\Exception $exception) {
            return response()->json([
                'status' => 500,
                'msg'   => 'لقد حدث خطأ ما'
            ], 500);
        }

    }


    public function datatable()
    {
        $pagination = Input::get('pagination');
        $query = Input::get('query');
        $search = $query['generalSearch'];


        if ($pagination['perpage'] == -1 || $pagination['perpage'] == null) {
            $pagination['perpage'] = 10;

        }
        $items = Faq::orderByDesc('created_at');

        $itemsCount = $items->count();
        $items = $items->with('translations')->take($pagination['perpage'])->skip($pagination['perpage'] * ($pagination['page'] - 1))->get();
        $pagination['total'] = $itemsCount;
        $pagination['pages'] = ceil($itemsCount / $pagination['perpage']);

        $data['meta'] = $pagination;
        $data['data'] = $items;
        return $data;
    }


}
