<?php

namespace App\Http\Controllers\panel;

use App\Admin;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Validator;

class AdminController extends Controller
{
    private function niceNames(){
        $niceNames = [];
        if (app()->isLocale('ar')){
            $niceNames = [
                'name'  => 'الإسم',
                'username'  => 'إسم المستخدم',
                'phone'  => 'رقم الهاتف',
                'email'  => 'البريد الإلكتروني',
                'password'  => 'كلمة المرور',
                'avatar'  => 'الصورة الشخصية',
            ];
        }
        return $niceNames;
    }

    public function index()
    {
        return view('panel.admins.index');
    }

    public function create()
    {
        return view('panel.admins.create');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $niceNames = $this->niceNames();

        $validator = Validator::make($data, [
           'name'   => 'required|string',
           'username'   => 'required|unique:admins,username',//|unique:admins,username
           'phone'   => 'required|numeric|unique:admins,phone',
           'email'   => 'required|email|unique:admins,email',
           'password'   => 'required|min:6|confirmed',
           'avatar'   => 'required|image',
        ]);
        $validator->setAttributeNames($niceNames);

        if ($validator->fails()){
            return response()->json([
                'status' => 422,
                'msg'   => $validator->errors()->first()
            ],422);
        }

        if ($avatar = $request->file('avatar')){
            $data['avatar'] = $avatar->store('admins');
        }
        $data['password'] = Hash::make($data['password']);
        Admin::create($data);
        return response()->json([
            'status' => 200,
            'msg'   => 'تمت العملية بنجاح'
        ],200);
    }

    public function edit($id)
    {

        $admin = Admin::findOrFail($id);
        return view('panel.admins.edit' , compact('admin'));
    }

    public function update(Request $request, $id)
    {
        $admin = Admin::findOrFail($id);
        $data = $request->all();
        $niceNames = $this->niceNames();

        $validator = Validator::make($data, [
            'name'   => 'required|string',
            'username'   => 'required|unique:admins,username,'.$admin->id,//|unique:admins,username
            'phone'   => 'required|numeric|unique:admins,phone,'.$admin->id,
            'email'   => 'required|email|unique:admins,email,'.$admin->id,
            'password'   => 'nullable|min:6|confirmed',
            'avatar'   => 'nullable|image',
        ]);
        $validator->setAttributeNames($niceNames);

        if ($validator->fails()){
            return response()->json([
                'status' => 422,
                'msg'   => $validator->errors()->first()
            ],422);
        }

        if ($avatar = $request->file('avatar')){
            Storage::delete($admin->avatar);
            $data['avatar'] = $avatar->store('admins');
        }
        if ($request->has('password')){
            $data['password'] = Hash::make($data['password']);
        }

        $admin->update($data);
        return response()->json([
            'status' => 200,
            'msg'   => 'تمت العملية بنجاح'
        ],200);
    }

    public function destroy($id)
    {
        try{
            $admin = Admin::findOrFail($id);

            if ($admin->avatar){
                Storage::delete($admin->avatar);
            }
            $admin->delete();
            return response()->json([
                'status' => 200,
                'msg'   => 'تمت العملية بنجاح'
            ], 200);
        }catch (\Exception $exception){
            return response()->json([
                'status' => 500,
                'msg'   => 'لقد حدث خطأ ما'
            ], 500);
        }

    }

    public function datatable()
    {
        $pagination = Input::get('pagination');
        $query = Input::get('query');
        $search = $query['generalSearch'];



        if ($pagination['perpage'] == -1 || $pagination['perpage'] == null) {
            $pagination['perpage'] = 10;

        }
        $items = Admin::orderByDesc('created_at');


        if ($search != null) {
            $items = $items->where('name', 'like', '%' . $search . '%')
                            ->orWhere('username', 'like', '%' . $search . '%')
                            ->orWhere('email', 'like', '%' . $search . '%')
                            ->orWhere('phone', 'like', '%' . $search . '%');
        }

        $itemsCount = $items->count();
        $items = $items->take($pagination['perpage'])->skip($pagination['perpage'] *($pagination['page']-1) )->get();
        $pagination['total'] = $itemsCount;
        $pagination['pages'] = ceil($itemsCount / $pagination['perpage']);

        $data['meta'] = $pagination;
        $data['data'] = $items;
        return $data;
    }


}
