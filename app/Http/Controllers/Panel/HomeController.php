<?php

namespace App\Http\Controllers\Panel;

use App\SearchLog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
//        $logCount = SearchLog::all()->count();
        return view('panel.index');
    }


    public function changeLang($locale)
    {
        session()->put('locale' , $locale);
        return redirect()->back();

    }



}
