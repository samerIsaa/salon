<?php

namespace App\Http\Controllers\Panel;

use App\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Validator;

class SliderController extends Controller
{

    private $niceNames = [
        'title' => 'العنوان',
        'description' => 'الوصف',
        'image' => 'صورة السلايدر',
        'isVisible' => 'قابل للعرض'
    ];


    private function validateRequest($data)
    {

        $validator = Validator::make($data, [
            'title' => 'required|string',
            'description' => 'required',
            'image' => 'required|image',
            'isVisible' => 'boolean',
        ]);

        $validator->setAttributeNames($this->niceNames);
        return $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = Slider::all();
        return view('panel.slider.index' , compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel.slider.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['isVisible'] = $request->has('isVisible') ? true : false;

        $validator = $this->validateRequest($data);


        if ($validator->fails()) {
            session()->flash('error', $validator->errors()->first());
            return back()->withInput();
        }


        if ($file = $request->file('image')) {
            $data['image'] = $file->store('sliders');
        }

        Slider::create($data);
        session()->flash('success', 'تمت العملية بنجاح');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id , Request $request)
    {
        if ($request->json()){
            try{
                $slider = Slider::findOrFail($id);
                if ($slider ->image) {
                    Storage::delete($slider ->image);
                }
                $slider ->delete();

                return response()->json([
                    'status'    => 200 ,
                    'msg'       => 'تم حذف السلايدر بنجاح'
                ],200);
            }catch (\Exception $exception){
                return response()->json([
                    'status'    => 500 ,
                    'msg'       => 'لقد حدث خطأ ما'
                ] , 500);
            }
        }
    }


    public function updateVisiblity($id , Request $request)
    {
        if ($request->json()){


            try{
                $slider = Slider::findOrFail($id);
                $slider->update(['isVisible' => $request->visible]);

                return response()->json([
                    'status'    => 200 ,
                    'msg'       => 'تم تعديل السلايدر بنجاح'
                ],200);
            }catch (\Exception $exception){
                return response()->json([
                    'status'    => 500 ,
                    'msg'       => 'لقد حدث خطأ ما'
                ] , 500);
            }
        }
    }
}
