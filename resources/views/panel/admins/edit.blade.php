@extends('panel.layout.master' , ['title' => 'تعديل مدير'])


@section('content_head')
    <div class="kt-subheader  kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">تعديل مدير</h3>
            </div>
        </div>
    </div>

@endsection

@section('content')
    <form class="kt-form" action="" method="post" id="kt_form_1">
        @csrf
        @method('PUT')
        <div class="row">

            <div class="col-md-8">

                <!--begin::Portlet-->
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                تعديل مدير
                            </h3>
                        </div>
                    </div>

                    <!--begin::Form-->

                    <div class="kt-portlet__body">
                        <div class="form-group">
                            <label>الاسم</label>
                            <input class="form-control m-input" type="text" name="name" placeholder="الاسم" value="{{ $admin->name }}">

                        </div>
                        <div class="form-group">
                            <label>اسم المستخدم</label>
                            <input class="form-control m-input" type="text" name="username"
                                   placeholder="اسم المستخدم"  value="{{ $admin->username }}">
                        </div>
                        <div class="form-group">
                            <label>رقم الجوال</label>
                            <input class="form-control m-input" type="text" name="phone"
                                   placeholder="رقم الجوال"  value="{{ $admin->phone }}">
                        </div>
                        <div class="form-group">
                            <label>البريد الإلكتروني</label>
                            <input class="form-control m-input" type="email" name="email"
                                   placeholder="البريد الإلكتروني"  value="{{ $admin->email }}">
                        </div>

                        <div class="form-group">
                            <label>كلمة المرور</label>
                            <input class="form-control m-input" type="password" name="password" id="password"
                                   placeholder="كلمة المرور">
                        </div>

                        <div class="form-group">
                            <label>تاكيد كلمة المرور</label>
                            <input class="form-control m-input" type="password" name="password_confirmation"
                                   placeholder="تاكيد كلمة المرور">
                        </div>

                    </div>


                    <!--end::Form-->
                </div>

                <!--end::Portlet-->

            </div>

            <div class="col-lg-4">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label"  style="width: 100%;">
                            <button type="submit"  style="width: 100%;" id="save" class="btn btn-success">حفظ</button>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet">
                    <div class="kt-portlet__body ">


                        <div class="form-group">
                            <label>الصورة الشخصية</label>
                            <div></div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="imgload" name="avatar">
                                <label class="custom-file-label" for="imgload" id="imgload">Choose file</label>
                            </div>
                        </div>

                        <div class="img-responsive">
                            <div class="imageEditProfile text-center">
                                <img src="@if($admin->avatar ) {{ url('image/'.$admin->avatar) }} @else {{ url('image/admins/default.jpg')  }} @endif" alt="" id="imgshow" style="max-width: 100%">
                            </div>
                        </div>

                    </div>
                </div>
            </div>


        </div>
    </form>

@endsection


@push('js')

    <script>

        var KTFormControls = function () {
            // Private functions

            var demo1 = function () {
                $("#kt_form_1").validate({
                    // define validation rules
                    rules: {
                        name: {
                            required: true,
                        },
                        username: {
                            required: true,
                        },
                        phone: {
                            required: true,
                            number: true,

                        },
                        email:{
                          required : true,
                          email: true
                        },
                        password: {
                            minlength: 6
                        },
                        password_confirmation: {
                            equalTo: "#password",
                        },
                        avatar:{
                            accept: "image/*"
                        }
                    },

                    //display error alert on form submit
                    invalidHandler: function (event, validator) {
                        var alert = $('#kt_form_1_msg');
                        alert.removeClass('kt--hide').show();
                    },

                    submitHandler: function (form) {
                        form[0].submit(); // submit the form
                    }
                });
            }


            return {
                // public functions
                init: function () {
                    demo1();
                }
            };
        }();

        jQuery(document).ready(function () {
            KTFormControls.init();
        });


        $('form#kt_form_1').submit(function (e)  {
            e.preventDefault();

            var fd = new FormData(this);

            if($(this).valid()){
                $('#save').attr('disabled', 'disabled')
                    .html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');

                $.ajax({
                    url : "{{ route('panel.admins.update' , $admin->id) }}",
                    method: 'POST',
                    contentType: false,
                    processData: false,
                    data: fd,
                    success: function (response) {
                        $('#save').removeAttr('disabled').html('حفظ');
                        swal.fire(response.msg, "", "success").then((res) => {
                            window.location = "{{ route('panel.admins.index') }}"
                        });
                    },
                    error: function (response) {
                        $('#save').removeAttr('disabled').html('حفظ');
                        swal.fire(response.responseJSON.msg, "", "error");

                    }
                });
            }
        });

    </script>
    <script>
        $("#imgload").change(function () {
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imgshow').attr('src', e.target.result);
                };
                reader.readAsDataURL(this.files[0]);
            }
        });

    </script>

@endpush
