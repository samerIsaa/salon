@extends('panel.layout.master' , ['title' => 'تعديل خدمة'])

@push('css')
    <link href="{{ asset('panelAssets/js/summernote/dist/summernote.css') }}" rel="stylesheet" type="text/css"/>
@endpush

@section('content_head')
    <div class="kt-subheader  kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">تعديل خدمة</h3>
            </div>
        </div>
    </div>

@endsection

@section('content')
    <form class="kt-form" action="" method="post" id="kt_form_1">
        @csrf
        @method('PUT')

        <div class="row">

            <div class="col-md-9">

                <!--begin::Portlet-->
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                تعديل مستخدم
                            </h3>
                        </div>
                    </div>

                    <!--begin::Form-->
                    <div class="kt-portlet__body">
                        <div class="form-group">
                            <label>الإسم</label>
                            <input class="form-control m-input" type="text" name="name"
                                   value="{{ $service->name }}" placeholder="الإسم">
                        </div>

                        <div class="form-group">
                            <label>الوصف</label>
                            <textarea class="summernote form-control" style="display:none!important;" id="kt_summernote_1"
                                      name="description" placeholder="الوصف">{{ $service->description }}</textarea>
                        </div>


                    </div>

                </div>
            </div>

            <div class="col-lg-3">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label"  style="width: 100%;">
                            <button type="submit"  style="width: 100%;" id="save" class="btn btn-brand">تعديل</button>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet">
                    <div class="kt-portlet__body ">


                        <div class="form-group">
                            <label>السعر</label>
                            <input class="form-control m-input" type="text" name="price"
                                   placeholder="السعر" value="{{ $service->price }}">
                        </div>


                    </div>
                </div>
                <div class="kt-portlet">
                    <div class="kt-portlet__body ">


                        <div class="form-group">
                            <label>الصورة</label>
                            <div></div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="imgload" name="image">
                                <label class="custom-file-label" for="imgload" id="imgload">Choose file</label>
                            </div>
                        </div>

                        <div class="img-responsive">
                            <div class="imageEditProfile">
                                <img src="@if($service->image) {{ url('image/' . $service->image) }} @endif" alt="" id="imgshow" style="max-width: 100%">
                            </div>
                        </div>

                    </div>
                </div>

            </div>


        </div>
    </form>

@endsection


@push('js')
    <script src="{{ asset('panelAssets/js/summernote/dist/summernote.js') }}" type="text/javascript"></script>
    <script src="{{ asset('panelAssets/js/summernote.min.js') }}" type="text/javascript"></script>

    <script>

        var KTFormControls = function () {
            // Private functions

            var demo1 = function () {
                $("#kt_form_1").validate({
                    rules: {
                        name: {
                            required: true,
                        },
                        price: {
                            required: true,
                            number: true,
                        },
                        description:{
                            required : true,
                        },
                        image:{
                            accept: "image/*"
                        }
                    },
                    //display error alert on form submit
                    invalidHandler: function (event, validator) {
                        var alert = $('#kt_form_1_msg');
                        alert.removeClass('kt--hide').show();
                    },

                    submitHandler: function (form) {
                        form[0].submit(); // submit the form
                    }
                });
            }


            return {
                // public functions
                init: function () {
                    demo1();
                }
            };
        }();

        jQuery(document).ready(function () {
            KTFormControls.init();
        });

        $('#kt_form_1').submit(function (e) {
            e.preventDefault();
            if($(this).valid()){
                $('#save').attr('disabled', 'disabled')
                    .html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');

                var fd = new FormData(this);


                $.ajax({
                    url : "{{ route('panel.services.update' , $service->id ) }}",
                    method: 'POST',
                    contentType: false,
                    processData: false,
                    data : fd,
                    success : function (response) {
                        $('#save').removeAttr('disabled').html('تعديل');
                        swal.fire({
                            type: 'success',
                            title: response.msg,
                            confirmButtonText: 'موافق'
                        }).then((result) => {
                            window.location = "{{ route('panel.services.index') }}";
                        });
                    },
                    error: function (response) {
                        $('#save').removeAttr('disabled').html('تعديل');
                        swal.fire(response.responseJSON.msg, "", "error");

                    }
                });

            }


        });


        $("#imgload").change(function () {
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imgshow').attr('src', e.target.result);
                };
                reader.readAsDataURL(this.files[0]);
            }
        });
    </script>


@endpush
