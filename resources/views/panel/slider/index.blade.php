@extends('panel.layout.master' , ['title' => 'جميع السلايدرات'])

@push('css')
    <link href="{{asset('panelAssets/css/fancybox.min.css')}}" rel="stylesheet" type="text/css"/>
@endpush

@section('content_head')
    <div class="kt-subheader  kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">جميع السلايدرات</h3>
            </div>
        </div>
    </div>

@endsection

@section('content')
    <div class="row">
        <div class="col-xl-12">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            جميع السلايدرات
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">

                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>الصورة</th>
                                    <th>العنوان</th>
                                    <th>الوصف</th>
                                    <th>معروض ؟</th>
                                    <th>العمليات</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($sliders)
                                    @foreach($sliders as $slider)
                                        <tr>
                                            <th scope="row">{{ $loop->index +1  }}</th>
                                            <td>
                                                <a href="{{ url('image/'.$slider->image) }}" data-fancybox>
                                                    <img src="{{ url('image/'.$slider->image.'/80x80') }}">
                                                </a>
                                            </td>
                                            <td>{{ $slider->title }}</td>
                                            <td>{{substr( $slider->description, 0, 512) . '...' }}</td>
                                            <td>
                                                <span class="kt-switch kt-switch--icon">
                                                    <label>
                                                        <input type="checkbox" id="visible"
                                                               data-url="{{ route('panel.slider.updateVisiblity' , $slider->id ) }}"
                                                               {{ $slider->isVisible ? "checked "  : ""  }} name="">
                                                        <span></span>
                                                    </label>
                                                </span>
                                            </td>
                                            <td>
                                                <a href="#"
                                                   data-url="{{ route('panel.slider.destroy' , $slider->id ) }}"
                                                   class="btn btn-sm btn-clean btn-icon btn-icon-md delete"
                                                   title="حذف">
                                                    <i class="fa fa-trash fa-2x"></i>
                                                </a>
                                            </td>
                                        </tr>

                                    @endforeach
                                @endif


                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!--end::Section-->

                </div>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>


@endsection


@push('js')
    <script src={{asset('panelAssets/js/fancybox.min.js')}}  type="text/javascript"></script>

    <script>
        $(document).on('change', '#visible', function (event) {
            var update_url = $(this).data('url');
            event.preventDefault();
            $.ajax({
                url: update_url,
                method: 'POST',
                data: {
                    visible: $(this)[0].checked ? '1' : '0'
                },
                type: 'json',
                success: function (response) {
                    if (response.status == 200) {
                        swal.fire({
                            title: response.msg,
                            type: 'success',
                            showCancelButton: false,
                            confirmButtonText: 'موافق',
                            dismiss: false
                        }).then(function (result) {
                            location.reload();
                        });
                    } else {
                        swal.fire("Error !", response.msg, "error");
                    }
                },
                error:
                    function (response) {
                        swal.fire(response.responseJSON.msg, '', "error");
                    }
            });
        });
    </script>
@endpush
