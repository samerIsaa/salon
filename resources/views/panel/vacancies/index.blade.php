@extends('panel.layout.master' , ['title' => 'الوظائف الشاغرة'])


@push('css')
    <link href="{{asset('panelAssets/css/bootstrap-select.css')}}" rel="stylesheet" type="text/css"/>
@endpush


@section('content_head')
    <div class="kt-subheader  kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">الوظائف الشاغرة</h3>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    الوظائف الشاغرة
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="javascript:;" data-toggle="modal" data-target=".add"
                           class="btn btn-success btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                            إضافة وظيفة شاغرة
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">


        <!--begin: Search Form -->
            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="row align-items-center">
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-input-icon kt-input-icon--left">
                                    <input type="text" class="form-control"
                                           placeholder="بحث..." id="generalSearch">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
																<span><i class="la la-search"></i></span>
															</span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="dropdown dropdown-inline">
                    <button type="button"
                            class="btn btn-hover-success btn-elevate-hover btn-icon btn-sm btn-icon-md btn-circle"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="flaticon-more-1"></i>
                    </button>
                    <div
                        class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item deletes"><i class="la la-trash"></i> حذف المحدد </a>
                    </div>
                </div>

            </div>

            <!--end: Search Form -->
        </div>
        <div class="kt-portlet__body kt-portlet__body--fit">

            <!--begin: Datatable -->
            <div class="kt-datatable text-center" id="ajax_data"></div>

            <!--end: Datatable -->
        </div>
    </div>


   @include('panel.vacancies.modal')

@endsection

@push('js')

    <script src="{{asset('panelAssets')}}/js/jquery.dataTables.js" type="text/javascript"></script>
    <script src={{asset('panelAssets/js/data-ajax.js')}}  type="text/javascript"></script>


    <script>

        window.data_url = '{{url('panel/vacancies/datatable')}}';

        window.columns = [
            {
                field: '',
                title: '',
                width: 40,
                sortable: false,
                // type: 'number',
                selector: {class: 'kt-checkbox--solid', name: 'admins'},
            },
            {
                field: 'title',
                title: "العنوان",
                'class': "text-center",
                textAlign: "center",
            },
            {
                field: 'Actions',
                title: "العمليات",
                sortable: false,
                overflow: 'visible',
                textAlign: "center",
                autoHide: false,
                template: function (data) {
                    var url = "{{ url('panel/vacancies/') }}/" + data.id;

                    return `
                        <input value=` + data.id + ` type="hidden" class="id">
						<div class="dropdown">
							<a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown">
                                <i class="la la-cog"></i>
                            </a>
						  	<div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="javascript:;" data-url="` + url + `" data-title="` + data.title + `"
                                        data-toggle="modal" data-target=".edit">
                                        <i class="la la-edit"></i>تعديل</a>
                <a class="dropdown-item delete" href="#" data-url="` + url + `"><i class="la la-times"></i>حذف</a>
						  	</div>
						</div>`;
                },

            }
        ];

        $('.kt-portlet__body .dropdown-menu').on('click', '.deletes', function () {

            var checkedItems = [];
            $.each($(".kt-checkbox--solid input[type='checkbox']:checked").not('.kt-checkbox--all'), function () {
                if ($(this).parents('tr').find('.id').val() !== undefined) {
                    checkedItems.push($(this).parents('tr').find('.id').val());
                }
            });

            if (checkedItems.length != 0) {
                swal.fire({
                    title: 'هل انت متاكد من عملية الحذف',
                    type: 'warning',
                    showCancelButton: true,
                    cancelButtonText: 'الغاء',
                    confirmButtonText: 'موافق'
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "{{ route('panel.vacancies.deleteSelected') }}",
                            method: "post",
                            data: {ids: checkedItems},
                            success: function (response) {
                                swal.fire({
                                    title: response.msg,
                                    type: 'success',
                                    showCancelButton: false,
                                    confirmButtonText: 'موافق',
                                    dismiss: false
                                }).then(function (result) {
                                    datatable.reload();
                                });
                            },
                            error: function (response) {
                                swal.fire(
                                    response.responseJSON.msg,
                                );
                            }
                        });

                    }
                });


            } else {
                swal.fire({
                    title: '@lang('panel.selectOneAtLeast')',
                    type: 'error',
                    showCancelButton: false,
                    confirmButtonText: '@lang('panel.close')',
                    dismiss: false
                }).then(function (result) {

                });
            }

        });

        $(".add").on('show.bs.modal', function (e) {

            $('.add').find("button#save").on('click', function () {


                $.ajax({
                    url: "{{ route('panel.vacancies.store') }}",
                    method: "POST",
                    data: $('.add').find("form").serialize(),
                    success: function (response) {
                        if (response.status == 200) {
                            swal.fire(
                                response.msg,
                            );
                            swal.fire({
                                title: response.msg,
                                type: 'success',
                                showCancelButton: false,
                                confirmButtonText: 'موافق',
                                dismiss: false
                            }).then(function (result) {
                                location.reload();
                            });
                        }
                    },
                    error: function (response) {
                        swal.fire(
                            response.responseJSON.msg,
                        );
                        swal.fire({
                            title: response.responseJSON.msg,
                            type: 'error',
                            showCancelButton: false,
                            confirmButtonText: 'موافق',
                            dismiss: false
                        });
                    }

                })


            });

        });
        $(".edit").on('show.bs.modal', function (e) {


            var url = $(e.relatedTarget).data('url');
            var title = $(e.relatedTarget).data('title');

            $('.edit').find("input[name=title]").val(title);

            $('.edit').find("button#save").on('click', function () {


                $.ajax({
                    url: url,
                    method: "post",
                    data: $('.edit').find('form').serialize(),
                    success: function (response) {
                        if (response.status == 200) {
                            swal.fire(
                                response.msg,
                            );
                            swal.fire({
                                title: response.msg,
                                type: 'success',
                                showCancelButton: false,
                                confirmButtonText: 'موافق',
                                dismiss: false
                            }).then(function (result) {
                                location.reload();
                            });
                        }
                    },
                    error: function (response) {
                        swal.fire(
                            response.responseJSON.msg,
                        );
                        swal.fire({
                            title: response.responseJSON.msg,
                            type: 'error',
                            showCancelButton: false,
                            confirmButtonText: 'موافق',
                            dismiss: false
                        });
                    }

                })


            });

        });

    </script>

@endpush
