<!--begin::Modal-->
<div class="modal fade add" id="kt_select2_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">إضافة</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>العنوان</label>
                        <input class="form-control m-input" type="text" name="title"
                               placeholder="العنوان">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="save">إضافة</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade edit" id="kt_select2_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">تعديل</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form>
                    @method('PUT')

                    <div class="form-group">
                        <label>العنوان</label>
                        <input class="form-control m-input" type="text" name="title"
                               placeholder="العنوان">
                    </div>


                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="save">تعديل</button>
            </div>
        </div>
    </div>
</div>
<!--end::Modal-->
