<!-- begin::Global Config(global config for global JS sciprts) -->
<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#5d78ff",
                "dark": "#282a3c",
                "light": "#ffffff",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": [
                    "#c5cbe3",
                    "#a1a8c3",
                    "#3d4465",
                    "#3e4466"
                ],
                "shape": [
                    "#f0f3ff",
                    "#d9dffa",
                    "#afb4d4",
                    "#646c9a"
                ]
            }
        }
    };
</script>

<!--begin:: Vendor Plugins -->
<script src="{{ asset('panelAssets') }}/js/jquery.js" type="text/javascript"></script>
<script src="{{ asset('panelAssets') }}/js/popper.js" type="text/javascript"></script>
<script src="{{ asset('panelAssets') }}/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{ asset('panelAssets') }}/js/js.cookie.js" type="text/javascript"></script>
<script src="{{ asset('panelAssets') }}/js/perfect-scrollbar.js" type="text/javascript"></script>
<script src="{{ asset('panelAssets') }}/js/sticky.min.js" type="text/javascript"></script>
<script src="{{ asset('panelAssets') }}/js/jquery.form.min.js" type="text/javascript"></script>
<script src="{{ asset('panelAssets') }}/js/jquery.blockUI.js" type="text/javascript"></script>
<script src="{{ asset('panelAssets') }}/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="{{ asset('panelAssets') }}/js/owl.carousel.js" type="text/javascript"></script>
<script src="{{ asset('panelAssets') }}/js/jquery.validate.js" type="text/javascript"></script>
<script src="{{ asset('panelAssets') }}/js/additional-methods.js" type="text/javascript"></script>
<script src="{{ asset('panelAssets') }}/js/jquery-validation.init.js" type="text/javascript"></script>
<script src="{{ asset('panelAssets') }}/js/scripts.bundle.js" type="text/javascript"></script>
<script src="{{ asset('panelAssets') }}/js/dashboard.js" type="text/javascript"></script>
<script src="{{ asset('panelAssets') }}/js/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
<script src="{{ asset('panelAssets') }}/js/sweetalert2.init.js" type="text/javascript"></script>
<script src="{{ asset('panelAssets/js/bootstrap-select.js') }}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/localization/messages_ar.min.js"
        integrity="sha256-EPgqOoVBNThJb8TtkiZe177cdDIaFu6CX7d5DzLCzZ4=" crossorigin="anonymous"></script>
{{--<script src="{{ asset('panelAssets') }}/plugins/general/wnumb/wNumb.js" type="text/javascript"></script>--}}
{{--<script src="{{ asset('panelAssets') }}/plugins/general/moment/min/moment.min.js" type="text/javascript"></script>--}}
{{--<script src="{{ asset('panelAssets') }}/plugins/general/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>--}}

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).on('click', '.delete', function (e) {
        e.preventDefault();
        var url = $(this).data('url');
        swal.fire({
            title: "هل انت متاكد من هذه العملية",
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: 'الغاء',
            confirmButtonText: 'نعم'
        }).then(function (result) {
            if (result.value) {

                $.ajax({
                    url: url,
                    method: "POST",
                    data: {
                        "_method": "Delete",
                    },
                    success: function (response) {
                        if (response.status == 200) {
                            swal.fire({
                                type: 'success',
                                title: response.msg,
                                confirmButtonText: 'موافق'
                            }).then(function (result) {
                                location.reload();
                            })
                        }
                    },
                    error: function (response) {
                        swal.fire(response.responseJSON.msg, '', "error");
                    }
                });

            }
        });


    });

    $('.kt-portlet__body .dropdown-menu').on('click', '.deletes', function () {

        var checkItems = [];
        $.each($(".kt-checkbox--solid input[type='checkbox']:checked").not(".kt-checkbox--solid input[type='checkbox']:checked .kt-checkbox--all"), function () {
            checkItems.push($(this).parents('tr').find('.id').val());
        });

        if (checkItems.length != 0) {
            let url = $(this).data('url');
            swal.fire({
                title: 'هل انت متاكد من عملية الحذف',
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: 'الغاء',
                confirmButtonText: 'تاكيد'
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: url,
                        method: "post",
                        data: {ids: checkItems},
                        success: function (response) {


                            swal.fire({
                                title: response.msg,
                                type: 'success',
                                showCancelButton: false,
                                confirmButtonText: 'اغلاق',
                                dismiss: false
                            }).then( (result) => {
                                datatable.reload();
                            });
                        },
                        error : function (response) {
                            swal.fire(
                                response.responseJSON.msg,
                            );
                        }
                    });

                }
            });


        } else {
            swal.fire({
                title: 'يرجى اختيار عنصر واحد على الاقل',
                type: 'error',
                showCancelButton: false,
                confirmButtonText: 'اغلاق',
                dismiss: false
            });
        }

    });


    @if(session()->has('success'))
    swal.fire("{{ session('success') }}", "", "success");
    @endif
    @if(session()->has('error'))
    swal.fire("{{ session('error') }}", "", "error");
    @endif
</script>


