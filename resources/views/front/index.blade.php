@extends('front.layout.master')



@section('content')

    <!-- Heroslider Area -->
    <div class="tm-heroslider-area">

        <div class="tm-heroslider-slider">

        @foreach($sliders as $slider)
            <!-- Heroslider Item -->
                <div class="tm-heroslider" data-bgimage="{{ url('image/' . $slider->image ) }}">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-7 col-md-9">
                                <div class="tm-heroslider-contentwrapper">
                                    <div class="tm-heroslider-content">
                                        <h1>{{ $slider->title }}</h1>
                                        <p>{{ $slider->description }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--// Heroslider Item -->
            @endforeach


        </div>

        <ul class="tm-heroslider-social">
{{--            <li><a href="{{ getSetting('facebook') }}" target="_blank" title="facebook"><i--}}
{{--                        class="ti ti-facebook"></i></a>--}}
{{--                <span class="tm-heroslider-social-tooltip">Facebook</span>--}}
{{--            </li>--}}
{{--            <li><a href="{{ getSetting('twitter') }}" target="_blank" title="twitter"><i class="ti ti-twitter-alt"></i></a>--}}
{{--                <span class="tm-heroslider-social-tooltip">Twitter</span>--}}
{{--            </li>--}}
            <li><a href="https://api.whatsapp.com/send?phone={{ getSetting('whatsapp') }}&text=السلام عليكم "
                   target="_blank" title="whatsapp"><i class="fa fa-whatsapp"></i></a>
                <span class="tm-heroslider-social-tooltip">whatsapp</span>
            </li>
            <li><a href="{{ getSetting('instagram') }}" target="_blank" title="instagram"><i
                        class="fa fa-instagram"></i></a>
                <span class="tm-heroslider-social-tooltip">Instagram</span>
            </li>

        </ul>

    </div>
    <!--// Heroslider Area -->

    <!-- Page Content -->

    <main class="page-content">

        <!-- About Area -->
        <section id="tm-about-area" class="tm-section tm-about-area tm-padding-section bg-white"
                 data-bgimage="{{ asset('frontAssets/images/about-bg-image.png') }}" data-white-overlay="8">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-6 col-lg-8 col-md-9 col-12">
                        <div class="tm-sectiontitle text-center">
                            <h2>{{ $about->title }}</h2>
                            <span class="tm-sectiontitle-divider">
                                    <img src="{{ asset('frontAssets/images/section-divider-icon.png') }}"
                                         alt="section divider">
                                </span>
                            <p>

                                {!!  $about->content !!}

                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--// About Area -->

        <!-- Service Area -->
        <div id="tm-service-area" class="tm-section tm-service-area tm-padding-section bg-white">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-6 col-lg-8 col-md-9 col-12">
                        <div class="tm-sectiontitle text-center">
                            <h2>خدماتنا</h2>
                            <span class="tm-sectiontitle-divider">
                                    <img src="{{ asset('frontAssets/images/section-divider-icon.png') }}"
                                         alt="section divider">
                                </span>
                            <p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص
                                العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد
                                الحروف التى يولدها التطبيق.</p>
                        </div>
                    </div>
                </div>

                <div class="row no-gutters tm-service-wrapper">
                    @foreach($services as $service)

                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="tm-service text-center tm-scrollanim">
                                <span class="tm-service-icon">
                                    <img src="{{ url('image/' . $service->image . '/100x100') }}"
                                         style="border-radius: 50%" alt="{{ $service->name }}">
                                </span>
                                <h5>{{ $service->name }}</h5>
                                <p>{{ string_limit(strip_tags($service->description) , 100 , '...') }}</p>
                                <a href="#" class="tm-readmore tm-readmore-dark">المزيد</a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <!--// Service Area -->

        <!-- Pricing & Compare Area -->
        <div class="tm-section tm-pricing-compare-area tm-padding-section bg-grey">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="tm-pricing">
                            <h3>سعر الخدمات</h3>
                            <p>وغالبا ما تسمى مستحضرات التجميل المطبقة على الوجه لتعزيز مظهره الماكياج أو الماكياج.
                                خدمات ماكياج السعر أدناه :</p>
                            <ul>
                                @foreach($services as $service)
                                    <li><span class="name">{{ $service->name }}</span> <span class="price">{{ $service->price }} ريال سعودي </span>
                                    </li>
                                @endforeach
                            </ul>
                            <a href="#appointment-area" class="tm-button hash-scroll-link">احجز الأن</a>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="tm-beforeafter">
                            <h3>Before after Services</h3>
                            <div class="tm-beforeafter-image">
                                <img src="{{ asset('frontAssets/images/beforeafter-before.jpg') }}" alt="before image">
                                <img src="{{ asset('frontAssets/images/beforeafter-after.jpg') }}" alt="after image">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--// Pricing & Compare Area -->

        <!-- Appointment Area -->
        <div id="appointment-area" class="tm-section tm-appointment-area tm-padding-section bg-white"
             data-bgimage="{{ asset('frontAssets/images/appointment-bg.png') }}">
            <div class="container">
                <div class="row text-center">
                    <div class="col-md-6">

                        <a href="https://api.whatsapp.com/send?phone={{ getSetting('whatsapp') }}&text=السلام عليكم "
                           target="_blank" title="whatsapp">
                            <h3 class="tm-heroslider-social-tooltip"><i class="fa fa-whatsapp"></i>whatsapp</h3>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <a href="{{ getSetting('instagram') }}" target="_blank" title="instagram">
                            <h3 class="tm-heroslider-social-tooltip"><i class="fa fa-instagram"></i>Instagram</h3>
                        </a>
                    </div>
                </div>

            </div>
        </div>
        <!--// Appointment Area -->

        <!-- Offers Area -->
        <div id="tm-shop-area" class="tm-section tm-products-area tm-padding-section bg-white"
             data-bgimage="{{ asset('frontAssets/images/products-bg-shape.png') }}" data-white-overlay="5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-6 col-lg-8 col-md-9 col-12">
                        <div class="tm-sectiontitle text-center">
                            <h2>أحدث عروضنا</h2>
                            <span class="tm-sectiontitle-divider">
                                    <img src="{{ asset('frontAssets/images/section-divider-icon.png') }}"
                                         alt="section divider">
                                </span>
                            <p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص
                                العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد
                                الحروف التى يولدها التطبيق.</p>
                        </div>
                    </div>
                </div>
                <div class="row mt-30-reverse">

                @foreach($offers as $offer)
                    <!-- Single Product -->
                        <div class="col-lg-4 col-md-6 col-sm-6 col-12 mt-30">
                            <div class="tm-product tm-scrollanim">
                                <div class="tm-product-topside">
                                    <img src="{{ url('image/' . $offer->image . '/370x400') }}"
                                         alt="{{ $offer->name }}">
                                </div>
                                <div class="tm-product-bottomside">
                                    <h6 class="tm-product-title"><a href="javascript:;">{{ $offer->name }}</a>
                                    </h6>
                                    <span class="tm-product-price"><del> {{ $offer->original_price }} ريال سعودي </del>  {{ $offer->offer_price }} ريال سعودي </span>
                                </div>
                            </div>
                        </div>
                        <!--// Single Product -->
                    @endforeach

                </div>
                <div class="tm-products-viewmore text-center mt-50">
                    <a href="{{ route('front.offers') }}" class="tm-button">المزيد</a>
                </div>
            </div>
        </div>
        <!--// Offers Area -->
        <!-- Testimonial Area -->
        <div class="tm-section tm-testimonial-area tm-padding-section bg-white" data-white-overlay="8"
             data-bgimage="assets/images/testimonial-bg-image.png">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-6 col-lg-8 col-md-9 col-12">
                        <div class="tm-sectiontitle text-center">
                            <h2>اخر الوظائف</h2>
                            <span class="tm-sectiontitle-divider">
                                    <img src="{{ asset('frontAssets/images/section-divider-icon.png') }}" alt="section divider">
                                </span>

                        </div>
                    </div>
                </div>
                <div class="tm-testimonial-wrapper tm-testimonial-slider tm-scrollanim">


                    @if ($vacancies)
                        @foreach($vacancies as $vacancy)

                            <div class="tm-testimonial">

                                <div class="tm-testimonial-author">
                                    <h6>{{ $vacancy->title }}</h6>

                                </div>
                            </div>

                        @endforeach
                    @endif



                </div>
            </div>
        </div>
        <!--// Testimonial Area -->

        <!-- Call To Action -->
        <div class="tm-section tm-calltoaction-area bg-grey">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="tm-calltoaction-image tm-scrollanim">
                            <img src="{{ asset('frontAssets/images/calltoaction-image.png') }}"
                                 alt="calltoaction image">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="tm-calltoaction-content tm-scrollanim">
                            <h3>تحتاج إلى أي نوع من مستحضرات التجميل! رجاء</h3>
                            <h4><a href="tel:{{ getSetting('phone') }}"><i class="ti ti-mobile"></i>
                                    Call: {{ getSetting('phone') }}</a></h4>
                            <h4><span>او</span></h4>
                            <a href="#tm-contactus-area" class="tm-button hash-scroll-link">تواصل معنا</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--// Call To Action -->

        <!-- Contact Us -->
        <div id="tm-contactus-area" class="tm-section tm-contact-area tm-padding-section bg-white"
             data-bgimage="{{ asset('frontAssets/images/contact-us-background.png') }}" data-white-overlay="8">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-6 col-lg-8 col-md-9 col-12">
                        <div class="tm-sectiontitle text-center">
                            <h2>تواصل معنا</h2>
                            <span class="tm-sectiontitle-divider">
                                    <img src="{{ asset('frontAssets/images/section-divider-icon.png') }}"
                                         alt="section divider">
                                </span>
                            <p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص
                                العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد
                                الحروف التى يولدها التطبيق.</p>
                        </div>
                    </div>
                </div>

                <div class="tm-contact-top tm-scrollanim">
                    <div class="row no-gutters">
                        <div class="col-lg-12">
                            <div class="tm-contact-address">
                                <h4>مكتب الشركة</h4>
                                <div class="tm-contact-addressblock">
                                    <b>العنوان</b>
                                    <p>{{ getSetting('address') }}</p>
                                </div>
                                <div class="tm-contact-addressblock">
                                    <b>البريد الالكتروني</b>
                                    <p><a href="mailto:info@example.com">{{ getSetting('email') }}</a></p>
                                </div>
                                <div class="tm-contact-addressblock">
                                    <b>رقم الهاتف</b>
                                    <p><a href="tel:{{ getSetting('phone') }}">{{ getSetting('phone') }}</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tm-contact-bottom tm-padding-section-top">
                    <div class="row mt-50-reverse">
                        <div class="col-lg-6">
                            <div class="tm-contact-formwrapper mt-50">
                                <h3>أرسل رسالتك</h3>
                                <form id="tm-contactform" action="{{ route('front.contact.send') }}"
                                      method="post" class="tm-contact-forminner tm-form">
                                    @csrf
                                    <div class="tm-form-inner">
                                        <div class="tm-form-field tm-form-fieldhalf">
                                            <input type="text" placeholder="الإسم (مطلوب)" name="name">
                                        </div>
                                        <div class="tm-form-field tm-form-fieldhalf">
                                            <input type="email" placeholder="البريد الإلكتروني (مطلوب)" name="email">
                                        </div>
                                        <div class="tm-form-field tm-form-fieldhalf">
                                            <input type="text" placeholder="رقم الجوال (مطلوب)" name="phone">
                                        </div>
                                        <div class="tm-form-field tm-form-fieldhalf">
                                            <input type="text" placeholder="الموضوع" name="subject">
                                        </div>
                                        <div class="tm-form-field">
                                                <textarea cols="30" rows="5" placeholder="الرسالة"
                                                          name="message"></textarea>
                                        </div>
                                        <div class="tm-form-field">
                                            <button type="submit" class="tm-button tm-button-block">ارسل</button>
                                        </div>
                                    </div>
                                </form>
                                <p class="form-messages"></p>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="tm-contact-map mt-50">
                                <h3>العثور على موقعنا</h3>
                                <div id="google-map" class="google-map"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--// Contact Us -->
    </main>

    <!--// Page Content -->

@endsection

@push('js')

    <script>
        // When the window has finished loading create our google map below
        google.maps.event.addDomListener(window, 'load', init);

        function init() {
            // Basic options for a simple Google Map
            // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
            var mapOptions = {
                // How zoomed in you want the map to start at (always required)
                zoom: 16,

                scrollwheel: false,

                // The latitude and longitude to center the map (always required)
                center: new google.maps.LatLng(parseFloat("{{ getSetting('latitude') }}"), parseFloat("{{ getSetting('longitude') }}")), // New York

                // How you would like to style the map.
                // This is where you would paste any style found on
                styles: [{
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#e9e9e9"
                    },
                        {
                            "lightness": 17
                        }
                    ]
                },
                    {
                        "featureType": "landscape",
                        "elementType": "geometry",
                        "stylers": [{
                            "color": "#f5f5f5"
                        },
                            {
                                "lightness": 20
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry.fill",
                        "stylers": [{
                            "color": "#ffffff"
                        },
                            {
                                "lightness": 17
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry.stroke",
                        "stylers": [{
                            "color": "#ffffff"
                        },
                            {
                                "lightness": 29
                            },
                            {
                                "weight": 0.2
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "geometry",
                        "stylers": [{
                            "color": "#ffffff"
                        },
                            {
                                "lightness": 18
                            }
                        ]
                    },
                    {
                        "featureType": "road.local",
                        "elementType": "geometry",
                        "stylers": [{
                            "color": "#ffffff"
                        },
                            {
                                "lightness": 16
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "geometry",
                        "stylers": [{
                            "color": "#f5f5f5"
                        },
                            {
                                "lightness": 21
                            }
                        ]
                    },
                    {
                        "featureType": "poi.park",
                        "elementType": "geometry",
                        "stylers": [{
                            "color": "#dedede"
                        },
                            {
                                "lightness": 21
                            }
                        ]
                    },
                    {
                        "elementType": "labels.text.stroke",
                        "stylers": [{
                            "visibility": "on"
                        },
                            {
                                "color": "#ffffff"
                            },
                            {
                                "lightness": 16
                            }
                        ]
                    },
                    {
                        "elementType": "labels.text.fill",
                        "stylers": [{
                            "saturation": 36
                        },
                            {
                                "color": "#333333"
                            },
                            {
                                "lightness": 40
                            }
                        ]
                    },
                    {
                        "elementType": "labels.icon",
                        "stylers": [{
                            "visibility": "off"
                        }]
                    },
                    {
                        "featureType": "transit",
                        "elementType": "geometry",
                        "stylers": [{
                            "color": "#f2f2f2"
                        },
                            {
                                "lightness": 19
                            }
                        ]
                    },
                    {
                        "featureType": "administrative",
                        "elementType": "geometry.fill",
                        "stylers": [{
                            "color": "#fefefe"
                        },
                            {
                                "lightness": 20
                            }
                        ]
                    },
                    {
                        "featureType": "administrative",
                        "elementType": "geometry.stroke",
                        "stylers": [{
                            "color": "#fefefe"
                        },
                            {
                                "lightness": 17
                            },
                            {
                                "weight": 1.2
                            }
                        ]
                    }
                ]
            };

            // Get the HTML DOM element that will contain your map
            // We are using a div with id="map" seen below in the <body>
            var mapElement = document.getElementById('google-map');

            // Create the Google Map using our element and options defined above
            var map = new google.maps.Map(mapElement, mapOptions);

            // Let's also add a marker while we're at it
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(parseFloat("{{ getSetting('latitude') }}"), parseFloat("{{ getSetting('longitude') }}")),
                map: map,
                title: 'Munu',
                icon: 'frontAssets/images/map-marker-icon.png',
                animation: google.maps.Animation.BOUNCE
            });
        }

    </script>

@endpush
