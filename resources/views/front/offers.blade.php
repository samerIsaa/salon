@extends('front.layout.master')



@section('content')

    <!-- Breadcrumb Area -->
    <div class="tm-breadcrumb-area tm-padding-section bg-grey" data-bgimage="{{ asset('frontAssets/images/breadcrumb-bg.jpg') }}">
        <div class="container">
            <div class="tm-breadcrumb">
                <h2>العروض</h2>
                <ul>
                    <li><a href="{{ url('/') }}">الرئيسية</a></li>
                    <li>العروض</li>
                </ul>
            </div>
        </div>
    </div>
    <!--// Breadcrumb Area -->

    <!-- Page Content -->
    <main class="page-content">

        <!-- Products Wrapper -->
        <div class="tm-products-area tm-section tm-padding-section bg-white">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-12">


                        <div class="tm-shop-products">
                            <div class="row mt-30-reverse">

                            @foreach($offers as $offer)
                                <!-- Single Product -->
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-12 mt-30">
                                        <div class="tm-product tm-scrollanim">
                                            <div class="tm-product-topside">
                                                <img src="{{ url('image/' . $offer->image . '/510x550') }}"
                                                     alt="{{ $offer->name }}">
                                            </div>
                                            <div class="tm-product-bottomside">
                                                <h6 class="tm-product-title"><a href="javascript:;">{{ $offer->name }}</a></h6>
                                                <span class="tm-product-price"><del> {{ $offer->original_price }} ريال سعودي </del>  {{ $offer->offer_price }} ريال سعودي </span>
                                            </div>
                                        </div>
                                    </div>
                                    <!--// Single Product -->
                            @endforeach



                            </div>
                        </div>
                        <div class="tm-pagination mt-50">
                            {{ $offers->links() }}
{{--                            <ul>--}}
{{--                                <li><a href="shop.html"><i class="ti ti-angle-right"></i></a></li>--}}
{{--                                <li class="is-active"><a href="shop.html">1</a></li>--}}
{{--                                <li><a href="shop.html">2</a></li>--}}
{{--                                <li><a href="shop.html">3</a></li>--}}
{{--                                <li><a href="shop.html"><i class="ti ti-angle-left"></i></a></li>--}}
{{--                            </ul>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--// Products Wrapper -->

    </main>
    <!--// Page Content -->


@endsection
