<div class="tm-footer">

    <div class="tm-footer-toparea tm-padding-section"
         data-bgimage="{{ asset('frontAssets/images/footer-bgimage.jpg') }}"
         data-white-overlay="9">
        <div class="container">
            <div class="widgets widgets-footer row">

                <div class="col-lg-6 col-md-6 col-12">
                    <div class="single-widget widget-info">
                        <a class="widget-info-logo" href="{{ url('/') }}"><img
                                src="{{ asset('logo.jpg') }}"
                                alt="white logo"></a>

                        <ul>
                            <li><a href="{{ getSetting('instagram') }}" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="https://api.whatsapp.com/send?phone={{ getSetting('whatsapp') }}&text=السلام" target="_blank"><i class="fa fa-whatsapp"></i></a></li>
{{--                            <li><a href="#"><i class="ti ti-pinterest"></i></a></li>--}}
{{--                            <li><a href="#"><i class="ti ti-linkedin"></i></a></li>--}}
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-12">
                    <div class="single-widget widget-quicklinks">
                        <h4 class="widget-title">روابط سريعة</h4>
                        <ul>
                            <li>
                                <a href="@if (\Request::is('/')) #tm-about-area @else {{ url('/') }}#tm-about-area @endif">من
                                    نحن</a></li>
                            <li>
                                <a href="@if (\Request::is('/')) #tm-service-area @else {{ url('/') }}#tm-service-area @endif">خدماتنا</a>
                            </li>
                            <li><a href="{{ route('front.offers') }}">العروض</a></li>
                            <li>
                                <a href="@if (\Request::is('/')) #tm-contactus-area @else {{ url('/') }}#tm-contactus-area @endif">تواصل
                                    معنا</a></li>

                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-12">
                    <div class="single-widget widget-hours">
                        <h4 class="widget-title">فترات العمل </h4>

                        <ul>
                            <li>بتوقيتنا من الساعه 10 صباحا الى 12 الليل </li>

                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="tm-footer-bottomarea">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-7">
                    <p class="tm-footer-copyright">CopyRights © {{ now()->year }}</p>
                </div>
                {{--                <div class="col-md-5">--}}
                {{--                    <div class="tm-footer-payment">--}}
                {{--                        <img src="{{ asset('frontAssets/images/payment-methods.png') }}" alt="payment methods">--}}
                {{--                    </div>--}}
                {{--                </div>--}}
            </div>
        </div>
    </div>
</div>
