<?php
return [
    'home'  => 'Home',
    'whoWeAre'  => 'Who we are',
    'privacyPolicy'  => 'Privacy policy',
    'usagePolicy'  => 'Usage policy',
    'contactUs'  => 'Contact us',
    'phone'  => 'Phone',
    'fax'  => 'Fax',
    'email'  => 'E-mail ',
    'faq'  => 'FAQs',
    'howWeWord'  => 'How we word',
    'mostFre'  => 'The most frequent',
    'questions'  => 'Questions',
    'search'  => 'Search Now',
    'result'  => 'Search result',
    'message'  => 'Message',
    'copyRight'  => 'Copyright © ' . now()->year,

    'resultNum' => 'We found :domains available domains and :social available username',

    'whatItIs'=>'What it is?',
    'readMore'=>'Read more',

    //header

    'headerTitle' => 'Explore the domain and your username now',
    'headerDescription' => 'Explore a domain available for your website and
                            use your own name on various social networks with greater ease and speed',
    'name' => 'Name',
    'domains' => 'Domains',
    'username' => 'Usernames',

    // Footer
    'links' => 'Links',
    'social' => 'Social media',
    'contact' => 'Contact info',

    // contact us
    'contacts'   => 'Contact',
    'us'   => 'Us',
    'requiredInformation'   => 'Required information',
    'sendMsg'   => 'Send Message',
    'contactInfo'   => 'Our contact information',
    'contactMsg'    => 'welcome Dear ! , Let\'s get to know more about your topic by entering the required information in the form. Below',
    'msgSent'    => 'Your message has been sent successfully. We will get back to you shortly',


    //filter
    'all'   => 'All',
    'available'   => 'Available',
    'reserved'   => 'Reserved',
];
?>
